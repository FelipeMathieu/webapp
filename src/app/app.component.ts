import { Component, OnDestroy, OnInit } from '@angular/core';
import { ProgressBarService } from './services/progress-bar.service';
import { Observable } from 'rxjs';
import { User, UsersQuery } from '@core/states/user';
import { map } from 'rxjs/operators';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  public progressBar$: Observable<boolean>;
  public isLogged$: Observable<boolean>;

  constructor(private progressBarService: ProgressBarService,
    private usersQuery: UsersQuery) { }

  ngOnInit(): void {
    this.setProgressBarListener();
    this.setLoginListener();
  }

  private setLoginListener(): void {
    this. isLogged$ = this.usersQuery.getCurrentUser$()
      .pipe(
        map((response: User) => !!response)
      );
  }

  private setProgressBarListener(): void {
    this.progressBar$ = this.progressBarService.getProgressBar$();
  }

  ngOnDestroy(): void {
    this.usersQuery.resetStore();
  }
}
