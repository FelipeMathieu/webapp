import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { LoginModule } from '@views/login/login.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { InterceptorsModule } from '@core/interceptors/interceptors.module';
import { ToastrModule } from 'ngx-toastr';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatToolbarModule } from '@angular/material/toolbar';
import { AppIconsModule } from '@shared/app-icons/app-icons.module';
import { MatButtonModule } from '@angular/material/button';

import { AppComponent } from './app.component';
import { UnauthorizedComponent } from './shared/unauthorized/unauthorized.component';
import { HeaderToolbarComponent } from './shared/header-toolbar/header-toolbar.component';

import { AkitaNgDevtools } from '@datorama/akita-ngdevtools';
import { environment } from '../environments/environment';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NotFoundComponent } from './shared/not-found/not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    UnauthorizedComponent,
    HeaderToolbarComponent,
    NotFoundComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    InterceptorsModule,
    LoginModule,
    environment.production ? [] : AkitaNgDevtools.forRoot(),
    ToastrModule.forRoot(),
    MatProgressBarModule,
    FlexLayoutModule,
    MatToolbarModule,
    AppIconsModule,
    MatButtonModule
  ],
  providers: [],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
