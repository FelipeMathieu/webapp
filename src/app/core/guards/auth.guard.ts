import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { User, UsersQuery } from '@core/states/user';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private usersQuery: UsersQuery, private router: Router) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.usersQuery.getCurrentUser$().pipe(
      map((response: User) => !!response),
      tap((response: boolean) => {
        if(!response) {
          this.router.navigate(['unauthorized']);
        }
      })
    );
  }
}
