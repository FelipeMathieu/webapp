import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpEvent, HttpRequest, HttpHandler } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpRequestHeader } from '../../utils/request-headers';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {
    private baseUrl: string = environment.baseUrl;

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const apiUrl: HttpRequest<any> = request.clone({
            url: `${this.baseUrl}/${request.url}`,
            withCredentials: true,
            headers: HttpRequestHeader
        });
        return next.handle(apiUrl);
    }
}