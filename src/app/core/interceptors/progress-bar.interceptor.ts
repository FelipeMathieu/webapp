import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpEvent, HttpRequest, HttpHandler } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ProgressBarService } from '@services/progress-bar.service';
import { finalize } from 'rxjs/operators';

@Injectable()
export class ProgressBarInterceptor implements HttpInterceptor {
    constructor(private progressBarService: ProgressBarService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        this.progressBarService.start();
        return next.handle(request)
            .pipe(
                finalize(() => this.progressBarService.stop())
            );
    }
}