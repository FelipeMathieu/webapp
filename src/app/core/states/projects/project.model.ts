import { DataObject } from '../utils/data-object.model';

export type Project = DataObject & {
    creatorId: number;
    name: string;
}