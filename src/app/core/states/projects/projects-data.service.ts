import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { Project } from './project.model';
import { UsersQuery } from '../user/users.query';
import { take, catchError, tap } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { User } from '../user';

@Injectable({
  providedIn: 'root'
})
export class ProjectsDataService {
  private route: string = 'projects';

  constructor(private httpClient: HttpClient,
    private usersQuery: UsersQuery,
    private toastrMessage: ToastrService) { }

  public getUserProjects$(): Observable<Project[]> {
    const currentUser: User = this.usersQuery.getCurrentUser();
    return !!currentUser ? this.getProjects$(+currentUser.id) : of([]);
  }

  public createNewProject$(newProject: Project): Observable<Project> {
    const currentUser: User = this.usersQuery.getCurrentUser();
    return !!currentUser ? this.createProject$(newProject, +currentUser.id) : of();
  }

  public updateProject$(newProject: Project): Observable<Project> {
    return this.httpClient.put<Project>(`${this.route}`, newProject)
      .pipe(
        take(1),
        catchError((err: HttpErrorResponse) => {
          this.toastrMessage.error(err.error.message);
          return throwError(err.error.message);
        }),
      );
  }

  public deleteProject$(projectId: number): Observable<Project> {
    return this.httpClient.put<Project>(`${this.route}/${projectId}/delete`, null)
      .pipe(
        take(1),
        catchError((err: HttpErrorResponse) => {
          this.toastrMessage.error(err.error.message);
          return throwError(err.error.message);
        }),
      );
  }

  private createProject$(newProject: Project, userId: number): Observable<Project> {
    return this.httpClient.post<Project>(`${this.route}`, {
        ...newProject,
        creatorId: userId
      })
      .pipe(
        take(1),
        catchError((err: HttpErrorResponse) => {
          this.toastrMessage.error(err.error.message);
          return throwError(err.error.message);
        }),
        tap((response: Project) => this.toastrMessage.success(`Project ${response.name} has been created!`))
      )
  }

  private getProjects$(userId: number): Observable<Project[]> {
    return this.httpClient.get<Project[]>(`users/${userId}/${this.route}`)
      .pipe(
        take(1),
        catchError((err: HttpErrorResponse) => {
          this.toastrMessage.error(err.error.message);
          return throwError(err.error.message);
        }),
      )
  }
}
