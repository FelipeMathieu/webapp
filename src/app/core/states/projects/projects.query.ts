import { Injectable } from '@angular/core';
import { combineQueries, QueryEntity, ID } from '@datorama/akita';
import { ProjectsState, ProjectsStore } from './projects.store';
import { Project } from './project.model';
import { UsersQuery } from '../user/users.query';
import { Observable } from 'rxjs';
import { User } from '../user';
import { map } from 'rxjs/operators';

export interface IMappedProjects {
    user: User;
    projects: Project[];
}

@Injectable({
    providedIn: 'root'
})
export class ProjectsQuery extends QueryEntity<ProjectsState, Project> {
    constructor(protected store: ProjectsStore,
        private usersQuery: UsersQuery) {
        super(store);
    }

    public getCurrentUserProjects$(): Observable<IMappedProjects> {
        return combineQueries([
            this.usersQuery.getCurrentUser$(),
            this.selectAll()
        ])
        .pipe(
            map(([currentUser, projects]) => {
                return {
                    user: { ...currentUser },
                    projects: projects.filter(p => +p.creatorId === +currentUser.id)
                        .sort((a, b) => a.name.localeCompare(b.name))
                } as IMappedProjects;
            })
        );
    }

    public getProjectById$(projectId: ID): Observable<Project> {
        return this.selectEntity(projectId);
    }

    public resetStore(): void {
        this.store.remove();
    }
}