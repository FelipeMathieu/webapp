import { Injectable } from '@angular/core';
import { ProjectsStore } from './projects.store';
import { ProjectsDataService } from './projects-data.service';
import { Observable } from 'rxjs';
import { Project } from './project.model';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProjectsService {
  constructor(private store: ProjectsStore,
    private projectsDataService: ProjectsDataService) { }

  public getCurrentUserProjects$(): Observable<Project[]> {
    return this.projectsDataService.getUserProjects$()
      .pipe(
        tap((response: Project[]) => this.store.upsertMany(response))
      );
  }

  public createNewProject$(newProject: Project): Observable<Project> {
    return this.projectsDataService.createNewProject$(newProject)
      .pipe(
        tap((response: Project) => this.store.upsert(response.id, response))
      );
  }

  public updateProject$(newProject: Project): Observable<Project> {
    return this.projectsDataService.updateProject$(newProject)
      .pipe(
        tap((response: Project) => this.store.upsert(response.id, response))
      );
  }

  public deleteProject(projectId: number): void {
    this.projectsDataService.deleteProject$(projectId)
      .subscribe(() => this.store.remove(projectId));
  }
}
