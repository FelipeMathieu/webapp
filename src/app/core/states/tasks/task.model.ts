import { DataObject } from '../utils/data-object.model';

export type Task = DataObject & {
    description: string;
    projectId: number;
}