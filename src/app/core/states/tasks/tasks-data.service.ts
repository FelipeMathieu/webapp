import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Task } from './task.model';
import { take, catchError, tap } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class TasksDataService {
  private route: string = 'tasks';

  constructor(private httpClient: HttpClient,
    private toastrMessage: ToastrService) { }

  public getTasksByProjectId$(projectId: number): Observable<Task[]> {
    return this.httpClient.get<Task[]>(`projects/${projectId}/${this.route}`)
      .pipe(
        take(1),
        catchError((err: HttpErrorResponse) => {
          this.toastrMessage.error(err.error.message);
          return throwError(err.error.message);
        }),
      );
  }

  public createNewTask$(newTask: Task): Observable<Task> {
    return this.httpClient.post<Task>(`${this.route}`, newTask)
      .pipe(
        take(1),
        tap(() => this.toastrMessage.success('Task has been created')),
        catchError((err: HttpErrorResponse) => {
          this.toastrMessage.error(err.error.message);
          return throwError(err.error.message);
        }),
      );
  }

  public closeTask$(taskId: number): Observable<Task> {
    return this.httpClient.put<Task>(`${this.route}/${taskId}/close`, null)
      .pipe(
        take(1),
        catchError((err: HttpErrorResponse) => {
          this.toastrMessage.error(err.error.message);
          return throwError(err.error.message);
        }),
      );
  }

  public deleteTask$(taskId: number): Observable<Task> {
    return this.httpClient.put<Task>(`${this.route}/${taskId}/delete`, null)
      .pipe(
        take(1),
        catchError((err: HttpErrorResponse) => {
          this.toastrMessage.error(err.error.message);
          return throwError(err.error.message);
        }),
      );
  }
}
