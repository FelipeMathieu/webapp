import { Injectable } from "@angular/core";
import { QueryEntity, combineQueries, ID } from '@datorama/akita';
import { Task } from './task.model';
import { TasksStore, TasksState } from './tasks.store';
import { ProjectsQuery } from '../projects/projects.query';
import { map } from 'rxjs/operators';
import { Project } from '../projects/project.model';
import { Observable } from 'rxjs';

export interface IMappedTasks {
    project: Project;
    tasks: Task[]
}

@Injectable({
    providedIn: 'root'
})
export class TasksQuery extends QueryEntity<TasksState, Task> {
    constructor(protected store: TasksStore,
        private projectsQuery: ProjectsQuery) {
        super(store);
    }

    public getMappedTasks$(projectId: ID): Observable<IMappedTasks> {
        return combineQueries([
                this.projectsQuery.getProjectById$(projectId),
                this.selectAll()
            ])
            .pipe(
                map(([project, tasks]) => {
                    return {
                        project,
                        tasks: tasks.filter(t => +t.projectId === +project.id)
                    }
                })
            )
    }

    public resetStore(): void {
        this.store.remove();
    }
}