import { Injectable } from '@angular/core';
import { TasksStore } from './tasks.store';
import { TasksDataService } from './tasks-data.service';
import { Observable } from 'rxjs';
import { Task } from './task.model';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TasksService {
  constructor(private store: TasksStore,
    private tasksDataService: TasksDataService) { }
  
  public getTasksProjectId$(projectId: number): Observable<Task[]> {
    return this.tasksDataService.getTasksByProjectId$(projectId)
      .pipe(
        tap((response: Task[]) => this.store.upsertMany(response))
      );
  }

  public createNewTask$(newTask: Task): Observable<Task> {
    return this.tasksDataService.createNewTask$(newTask)
      .pipe(
        tap((response: Task) => this.store.upsert(response.id, response))
      );
  }

  public closeTask(taskId: number): void {
    this.tasksDataService.closeTask$(taskId)
      .subscribe((response: Task) => this.store.upsert(response.id, response));
  }

  public deleteTask(taskId: number): void {
    this.tasksDataService.deleteTask$(taskId)
      .subscribe(() => this.store.remove(taskId));
  }
}
