import { Injectable } from '@angular/core';
import { EntityState, StoreConfig, EntityStore } from '@datorama/akita';
import { Task } from './task.model';

export interface TasksState extends EntityState<Task> { }

@Injectable({
    providedIn: 'root'
})
@StoreConfig({
    name: 'tasks'
})
export class TasksStore extends EntityStore<TasksState, Task> {
    constructor() {
        super();
    }
}