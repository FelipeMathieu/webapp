import { DataObject } from '../utils/data-object.model';

export type User = DataObject & {
    name: string;
    email: string;
}

export type LoginUI = {
    isLogin: boolean;
}