import { createHttpFactory, HttpMethod, mockProvider, SpectatorHttp, SpyObject } from '@ngneat/spectator';
import { ToastrService } from 'ngx-toastr';
import { UsersDataService } from './users-data.service';

describe('UserDataService', () => {
  let spectator: SpectatorHttp<UsersDataService>;
  let toastrService: SpyObject<ToastrService>;
  const email: string = 'hansolo@millenium.com';

  const createService = createHttpFactory({
    service: UsersDataService,
    providers: [
      mockProvider(ToastrService)
    ]
  });

  beforeEach(() => {
    spectator = createService();
    toastrService = spectator.inject<ToastrService>(ToastrService);
  });

  afterEach(() => {
    spectator.flushAll([], [{}]);
  });

  it('should request get user by email', () => {
    spectator.service.getUserByEmail$(email).subscribe();
    spectator.expectOne(`/users/${email}`, HttpMethod.GET);
  });

  it('should throw an error', () => {
    spectator.service.getUserByEmail$(email).subscribe();
    spectator.expectOne(`/users/${email}`, HttpMethod.GET).error(new ErrorEvent('fail test', {
      message : 'A monkey is throwing bananas at me!',
    }));
    expect(toastrService.error).toHaveBeenCalledOnceWith('A monkey is throwing bananas at me!');
  });
});
