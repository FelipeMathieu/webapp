import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Observable, throwError } from 'rxjs';
import { User } from './user.model';
import { catchError, take, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsersDataService {
  private route: string = 'users';

  constructor(private httpClient: HttpClient,
    private toastrMessage: ToastrService) { }

  public getUserByEmail$(email: string): Observable<User> {
    return this.httpClient.get<User>(`${this.route}/${email}`)
      .pipe(
        take(1),
        catchError((err: HttpErrorResponse) => {
          this.toastrMessage.error(err.error.message);
          return throwError(err.error.message);
        })
      )
  }

  public createNewUser$(newUser: User): Observable<User> {
    return this.httpClient.post<User>(`${this.route}`, newUser)
      .pipe(
        take(1),
        tap((response: User) => this.toastrMessage.success(`Welcome ${response.name}`)),
        catchError((err: HttpErrorResponse) => {
          this.toastrMessage.error(err.error.message);
          return throwError(err.error.message);
        })
      )
  }

  public login$(user: User): Observable<User> {
    return this.httpClient.post<User>(`${this.route}/login`, user)
      .pipe(
        take(1),
        tap((response: User) => this.toastrMessage.success(`Welcome ${response.name}`)),
        catchError((err: HttpErrorResponse) => {
          this.toastrMessage.error(err.error.message);
          return throwError(err.error.message);
        })
      );
  }
}
