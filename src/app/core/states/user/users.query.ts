import { Injectable } from "@angular/core";
import { EntityUIQuery, QueryEntity } from "@datorama/akita";
import { UsersState, UsersStore, LoginUIState } from './users.store';
import { Observable } from 'rxjs';
import { User, LoginUI } from './user.model';

@Injectable({
    providedIn: 'root'
})
export class UsersQuery extends QueryEntity<UsersState, User> {
    ui: EntityUIQuery<LoginUIState, LoginUI>;

    constructor(protected store: UsersStore) {
        super(store);
        this.createUIQuery();
    }

    public getCurrentUser$(): Observable<User> {
        return this.selectFirst();
    }

    public getCurrentUser(): User {
        const state: UsersState = this.store.getValue();
        const id: number = !!state.ids ? state.ids[0] : 0;
        return !!state.entities ? state.entities[id] : null;
    }

    public resetStore(): void {
        this.store.remove();
        this.updateUI(true);
    }

    public updateUI(isLogin: boolean): void {
        this.store.update({
            ui: {
                isLogin
            }
        });
    }

    public getLoginUI$(): Observable<boolean> {
        return this.select(state => state.ui.isLogin);
    }
}