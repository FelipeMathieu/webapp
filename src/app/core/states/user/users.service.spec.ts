import { createServiceFactory, mockProvider, SpectatorService, SpyObject } from '@ngneat/spectator';
import { UsersService } from './users.service';
import { UsersStore } from './users.store';
import { UsersDataService } from './users-data.service';
import { of } from 'rxjs';
import { User } from './user.model';

describe('UsersService', () => {
  let spectator: SpectatorService<UsersService>;
  let usersDataService: SpyObject<UsersDataService>;
  let store: SpyObject<UsersStore>;
  const email: string = 'hansolo@millenium.com';
  const user: User = {
    name: 'Han Solo',
    email,
    id: 1,
    createdDate: new Date(Date.now()),
    endDate: new Date(Date.now()),
    isDeleted: false
  };

  const createService = createServiceFactory({
    service: UsersService,
    providers: [
      mockProvider(UsersStore),
      mockProvider(UsersDataService)
    ]
  });

  beforeEach(() => {
    spectator = createService();
    usersDataService = spectator.inject<UsersDataService>(UsersDataService);
    store = spectator.inject<UsersStore>(UsersStore);
    setUsersDataServiceResponse();
  });

  it('should request and send current user to the store', () => {
    spectator.service.getUserByEmail(email);
    expect(usersDataService.getUserByEmail$).toHaveBeenCalledOnceWith(email);
    expect(store.upsert).toHaveBeenCalledOnceWith(user.id, user);
  });

  function setUsersDataServiceResponse(): void {
    usersDataService.getUserByEmail$.and.returnValue(of(user));
  }
});
