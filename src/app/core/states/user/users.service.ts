import { Injectable } from '@angular/core';
import { UsersDataService } from './users-data.service';
import { UsersStore } from './users.store';
import { User } from './user.model';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  constructor(private store: UsersStore,
    private usersDataService: UsersDataService) { }

  public getUserByEmail(email: string): void {
    this.usersDataService.getUserByEmail$(email)
      .subscribe((response: User) => this.store.upsert(response.id, response));
  }

  public createNewUser$(newUser: User): Observable<User> {
    return this.usersDataService.createNewUser$(newUser)
      .pipe(
        tap((response: User) => this.store.upsert(response.id, response))
      );
  }

  public login$(user: User): Observable<User> {
    return this.usersDataService.login$(user)
      .pipe(
        tap((response: User) => this.store.upsert(response.id, response))
      );
  }
}
