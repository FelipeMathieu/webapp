import { Injectable } from '@angular/core';
import { EntityState, EntityStore, StoreConfig, EntityUIStore } from '@datorama/akita';
import { LoginUI, User } from './user.model';

export interface UsersState extends EntityState<User> {
    ui: {
        isLogin: boolean
    }
}

export interface LoginUIState extends EntityState<LoginUI> { }

@Injectable({
    providedIn: 'root'
})
@StoreConfig({
    name: 'users'
})
export class UsersStore extends EntityStore<UsersState, User> {
    ui: EntityUIStore<LoginUIState, LoginUI>;

    constructor() {
        super({
            ui: {
                isLogin: true
            }
        });
        this.createUIStore();
    }
}