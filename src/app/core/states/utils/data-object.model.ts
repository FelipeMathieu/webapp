import { ID } from "@datorama/akita";

export type DataObject = {
    id: ID;
    createdDate: Date;
    endDate: Date;
    isDeleted: boolean;
}