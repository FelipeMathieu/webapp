import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProgressBarService {
    private progressBar: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

    constructor() { }

    public start(): void {
        this.progressBar.next(true);
    }

    public stop(): void {
        this.progressBar.next(false);
    }

    public getProgressBar$(): Observable<boolean> {
        return this.progressBar.asObservable();
    }
}
