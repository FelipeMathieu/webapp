import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {
  faArrowAltCircleLeft,
  faCheckSquare,
  faEdit,
  faFileAlt,
  faPlus,
  faSave,
  faSignInAlt,
  faSignOutAlt,
  faTrashAlt,
  faUserPlus,
  faWindowClose,
} from '@fortawesome/free-solid-svg-icons';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    FontAwesomeModule
  ],
  exports: [
    FontAwesomeModule
  ]
})
export class AppIconsModule {
  constructor(library: FaIconLibrary) {
    library.addIcons(faSignInAlt,
      faUserPlus,
      faArrowAltCircleLeft,
      faSignOutAlt,
      faFileAlt,
      faPlus,
      faTrashAlt,
      faCheckSquare,
      faEdit,
      faSave,
      faWindowClose
    );
  }
}
