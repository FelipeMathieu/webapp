import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProjectsQuery } from '@core/states/projects';
import { TasksQuery } from '@core/states/tasks';
import { UsersQuery } from '@core/states/user';
import { ProgressBarService } from '@services/progress-bar.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-header-toolbar',
  templateUrl: './header-toolbar.component.html',
  styleUrls: ['./header-toolbar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderToolbarComponent implements OnInit, OnDestroy {
  public progressBar$: Observable<boolean>;

  constructor(private progressBarService: ProgressBarService,
    private router: Router,
    private usersQuery: UsersQuery,
    private projectsQuery: ProjectsQuery,
    private tasksQuery: TasksQuery) { }

  ngOnInit(): void {
    this.setProgressBarListener();
  }

  public logout(): void {
    this.usersQuery.resetStore();
    this.router.navigate(['login']);
  }

  private setProgressBarListener(): void {
    this.progressBar$ = this.progressBarService.getProgressBar$();
  }

  ngOnDestroy(): void {
    this.projectsQuery.resetStore();
    this.tasksQuery.resetStore();
  }
}
