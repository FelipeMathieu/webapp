import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IconnedButtonComponent } from './iconned-button.component';

describe('IconnedButtonComponent', () => {
  let component: IconnedButtonComponent;
  let fixture: ComponentFixture<IconnedButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IconnedButtonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IconnedButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
