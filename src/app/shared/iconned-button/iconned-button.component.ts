import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ProgressBarService } from '@services/progress-bar.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-iconned-button',
  templateUrl: './iconned-button.component.html',
  styleUrls: ['./iconned-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IconnedButtonComponent implements OnInit {
  @Input() text: string = '';
  @Input() iconName: string = '';
  @Input() isDisabled: boolean = false;
  @Input() buttonColor: string = '';
  @Output() buttonClick: EventEmitter<void> = new EventEmitter<void>();

  public progressBar$: Observable<boolean>;

  constructor(private progressBarService: ProgressBarService) { }

  ngOnInit(): void {
    this.setProgressBarListener();
  }

  public onClick(): void {
    this.buttonClick.emit();
  }

  private setProgressBarListener(): void {
    this.progressBar$ = this.progressBarService.getProgressBar$();
  }
}
