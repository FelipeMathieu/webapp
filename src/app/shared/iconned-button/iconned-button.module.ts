import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppIconsModule } from '../app-icons/app-icons.module';
import { MatButtonModule } from '@angular/material/button';
import { FlexLayoutModule } from '@angular/flex-layout';

import { IconnedButtonComponent } from './iconned-button.component';

@NgModule({
  declarations: [
    IconnedButtonComponent
  ],
  imports: [
    CommonModule,
    AppIconsModule,
    MatButtonModule,
    FlexLayoutModule,
  ],
  exports: [
    IconnedButtonComponent
  ]
})
export class IconnedButtonModule { }
