import { HttpHeaders } from "@angular/common/http";

export const HttpRequestHeader: HttpHeaders = new HttpHeaders().set('Content-Type','application/json');