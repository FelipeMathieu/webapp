import { Component, OnInit } from '@angular/core';
import { IMappedProjects, ProjectsQuery, ProjectsService } from '@core/states/projects';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-dashboard-screen',
  templateUrl: './dashboard-screen.component.html',
  styleUrls: ['./dashboard-screen.component.scss']
})
export class DashboardScreenComponent implements OnInit {
  public projects$: Observable<IMappedProjects>;

  constructor(private projectsService: ProjectsService,
    private projectsQuery: ProjectsQuery) { }

  ngOnInit(): void {
    this.loadCurrentUserProjects();
    this.setCurrentUserProjectsListener();
  }

  private loadCurrentUserProjects(): void {
    this.projectsService.getCurrentUserProjects$().subscribe();
  }

  private setCurrentUserProjectsListener(): void {
    this.projects$ = this.projectsQuery.getCurrentUserProjects$();
  }
}
