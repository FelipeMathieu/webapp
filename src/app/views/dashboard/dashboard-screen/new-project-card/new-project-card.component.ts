import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormControl, FormBuilder } from '@angular/forms';
import { Project, ProjectsService } from '@core/states/projects';
import { ProgressBarService } from '@services/progress-bar.service';
import { Observable } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

@Component({
  selector: 'app-new-project-card',
  templateUrl: './new-project-card.component.html',
  styleUrls: ['./new-project-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NewProjectCardComponent implements OnInit {
  public control: FormControl;
  public progressBar$: Observable<boolean>;

  constructor(private fb: FormBuilder,
    private progressBarService: ProgressBarService,
    private projectsService: ProjectsService) { }

  ngOnInit(): void {
    this.buildFormControl();
    this.setProgressBarListener();
  }

  public createNewProject(): void {
    if(!!this.control.value) {
      const newProject = {
        name: this.control.value,
      } as Project;
      this.enableControl(false)
      this.projectsService.createNewProject$(newProject)
        .pipe(catchError(() => {
          this.resetControl();
          return null;
        }))
        .subscribe(() => this.resetControl());
    }
  }

  private resetControl(): void {
    this.control.reset();
    this.enableControl(true);
  }

  private setProgressBarListener(): void {
    this.progressBar$ = this.progressBarService.getProgressBar$();
  }

  private enableControl(enable: boolean): void {
    enable
      ? this.control.enable({
          onlySelf: false,
          emitEvent: true
        })
      : this.control.disable({
          onlySelf: false,
          emitEvent: true
        });
  }

  private buildFormControl(): void {
    this.control = this.fb.control(null);
  }
}
