import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-no-project',
  templateUrl: './no-project.component.html',
  styleUrls: ['./no-project.component.scss']
})
export class NoProjectComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
