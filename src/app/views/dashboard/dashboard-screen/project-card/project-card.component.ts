import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { FormControl, FormBuilder, Validators } from '@angular/forms';
import { Project, ProjectsService } from '@core/states/projects';
import { IMappedTasks, Task, TasksQuery, TasksService } from '@core/states/tasks';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-project-card',
  templateUrl: './project-card.component.html',
  styleUrls: ['./project-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectCardComponent implements OnInit {
  @Input() project: Project;

  public tasks$: Observable<IMappedTasks>;
  public control: FormControl;
  public updateNameControl: FormControl;
  public isEditing: boolean = false;

  constructor(private fb: FormBuilder,
    private tasksService: TasksService,
    private tasksQuery: TasksQuery,
    private projectsService: ProjectsService) { }

  ngOnInit(): void {
    this.buildControl();
    this.setTasksListListener();
    this.loadProjectTasks();
  }

  public createNewTask() {
    if(!!this.control.value) {
      const newTask = {
        description: this.control.value,
        projectId: +this.project.id
      } as Task;
      this.enableControl(false);
      this.tasksService.createNewTask$(newTask)
        .subscribe(() => this.resetControl());
    }
  }

  public getToDoTasks(tasks: Task[]): Task[] {
    return tasks.filter(t => !t.endDate);
  }

  public getDoneTasks(tasks: Task[]): Task[] {
    return tasks.filter(t => !!t.endDate);
  }

  public removeProject(): void {
    this.projectsService.deleteProject(+this.project.id);
  }

  public updateProjectName(): void {
    const newProject: Project = {
      ...this.project,
      name: this.updateNameControl.value
    };
    this.projectsService.updateProject$(newProject)
      .subscribe((response: Project) => {
        this.isEditing = !this.isEditing;
        this.updateNameControl.setValue(response.name);
      });
  }

  private resetControl(): void {
    this.control.reset();
    this.enableControl(true)
  }

  private loadProjectTasks(): void {
    this.tasksService.getTasksProjectId$(+this.project.id)
      .subscribe();
  }

  private buildControl(): void {
    this.control = this.fb.control(null);
    this.updateNameControl = this.fb.control(this.project.name, Validators.required);
  }

  private setTasksListListener(): void {
    this.tasks$ = this.tasksQuery.getMappedTasks$(this.project.id);
  }

  private enableControl(enable: boolean): void {
    enable
      ? this.control.enable({
          onlySelf: false,
          emitEvent: true
        })
      : this.control.disable({
          onlySelf: false,
          emitEvent: true
        });
  }
}
