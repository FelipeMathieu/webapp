import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { Task, TasksService } from '@core/states/tasks';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TaskListComponent implements OnInit {
  @Input() tasks: Task[];
  @Input() closedTasks: boolean = false;

  public showTaskIcon: {} = {};

  constructor(private tasksService: TasksService) { }

  ngOnInit(): void {
  }

  public shouldShowIcons(task: Task): boolean {
    return this.showTaskIcon[task.id];
  } 

  public showIcons(task: Task, show: boolean): void {
    this.showTaskIcon[task.id] = show;
  }

  public closeTask(task: Task): void {
    this.showTaskIcon[task.id] = false;
    this.tasksService.closeTask(+task.id);
  }

  public removeTask(task: Task): void {
    this.showTaskIcon[task.id] = false;
    this.tasksService.deleteTask(+task.id);
  }
}
