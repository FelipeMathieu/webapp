import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { IconnedButtonModule } from '@shared/iconned-button/iconned-button.module';
import { AppIconsModule } from '@shared/app-icons/app-icons.module';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatCheckboxModule } from '@angular/material/checkbox';

import { DashboardScreenComponent } from './dashboard-screen/dashboard-screen.component';
import { NewProjectCardComponent } from './dashboard-screen/new-project-card/new-project-card.component';
import { ProjectCardComponent } from './dashboard-screen/project-card/project-card.component';
import { NoProjectComponent } from './dashboard-screen/no-project/no-project.component';
import { TaskListComponent } from './dashboard-screen/project-card/task-list/task-list.component';

@NgModule({
  declarations: [
    DashboardScreenComponent,
    NewProjectCardComponent,
    ProjectCardComponent,
    NoProjectComponent,
    TaskListComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    MatCardModule,
    MatInputModule,
    IconnedButtonModule,
    AppIconsModule,
    MatTooltipModule,
    MatCheckboxModule
  ]
})
export class DashboardModule { }
