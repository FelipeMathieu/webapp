import { LoginCardComponent } from './login-card.component';
import { Spectator, createComponentFactory, SpyObject, mockProvider } from '@ngneat/spectator';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { UsersQuery } from '@core/states/user';

describe('LoginCardComponent', () => {
  let spectator: Spectator<LoginCardComponent>;
  let usersQuery: SpyObject<UsersQuery>;

  const createComponent = createComponentFactory({
    component: LoginCardComponent,
    providers: [
      FormBuilder,
      mockProvider(UsersQuery),
    ],
    imports: [
      ReactiveFormsModule
    ],
    shallow: true,
    detectChanges: false,
  });

  beforeEach(() => {
    spectator = createComponent();
    usersQuery = spectator.inject<UsersQuery>(UsersQuery);
  });

  it('should create mat card element', () => {
    expect(getMatCardElement()).toBeVisible();
  });

  describe('When component have been initiated', () => {
    beforeEach(() => spectator.detectChanges());

    it('should create form', () => {
      const controls = Object.keys(spectator.component.form.controls);
      expect(controls.length).toBe(2);
    });

    it('should render email input', () => {
      expect(getEmailInputElement()).toBeVisible();
    });

    it('should render password input', () => {
      expect(getPasswordInputElemnt()).toBeVisible();
    });

    it('should render login button', () => {
      expect(getLoginButton()).toBeVisible();
    });

    it('should render sign up button', () => {
      expect(getSignUpButton()).toBeVisible();
    });

    it('should disable login button if email and password are invalid', () => {
      expect(getLoginButton()).toBeDisabled();
    });

    xit('should enable login button if email and password are valid', async() => {
      const form = spectator.component.form;
      form.get('email').setValue('hansolo@millenium.com');
      form.get('password').setValue('1234');
      spectator.detectChanges();
      expect(getLoginButton()).not.toBeDisabled();
    });
  });

  describe('When going to sign up card', () => {
    it('should call UI update', () => {
      spectator.click(getSignUpButton());
      expect(usersQuery.updateUI).toHaveBeenCalledTimes(1);
    });
  });

  const getMatCardElement = () => spectator.query('mat-card');
  const getEmailInputElement = () => spectator.query('#email-input');
  const getPasswordInputElemnt = () => spectator.query('#password-input');
  const getLoginButton = () => spectator.query('#login-button');
  const getSignUpButton = () => spectator.query('#sign-up-button');
});
