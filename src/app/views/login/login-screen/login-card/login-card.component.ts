import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User, UsersQuery, UsersService } from '@core/states/user';

@Component({
  selector: 'app-login-card',
  templateUrl: './login-card.component.html',
  styleUrls: ['./login-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginCardComponent implements OnInit {
  public form: FormGroup;

  constructor(private fb: FormBuilder,
    private usersQuery: UsersQuery,
    private usersService: UsersService,
    private router: Router) { }

  ngOnInit(): void {
    this.buildForm();
  }

  public setSignUpCard(): void {
    this.usersQuery.updateUI(false);
  }

  public loginUser(): void {
    if(this.form.valid) {
      const user: User = { ...this.form.value };
      this.usersService.login$(user)
        .subscribe(() => this.router.navigate(['dashboard']));
    } else {
      this.form.updateValueAndValidity({
        onlySelf: false,
        emitEvent: true
      });
    }
  }

  private buildForm(): void {
    this.form = this.fb.group({
      email: this.fb.control(null, [
        Validators.required,
        Validators.email,
      ]),
      password: this.fb.control(null, Validators.required)
    })
  }
}
