import { LoginScreenComponent } from './login-screen.component';
import { createComponentFactory, mockProvider, Spectator, SpyObject } from '@ngneat/spectator';
import { UsersQuery } from '@core/states/user';
import { BehaviorSubject } from 'rxjs';

describe('LoginScreenComponent', () => {
  let spectator: Spectator<LoginScreenComponent>;
  let usersQuery: SpyObject<UsersQuery>;
  const uiResponse$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);

  const createComponent = createComponentFactory({
    component: LoginScreenComponent,
    providers: [
      mockProvider(UsersQuery)
    ],
    shallow: true,
    detectChanges: false
  });

  beforeEach(() => {
    spectator = createComponent();
    usersQuery = spectator.inject<UsersQuery>(UsersQuery);
    setUIInitialState();
    spectator.detectChanges();
  });

  it('should create component', () => {
    expect(getComponentBody()).toBeVisible();
  });

  it('should render login card element', () => {
    expect(getLoginCardElement()).toBeVisible();
  });

  function setUIInitialState(): void {
    usersQuery.getLoginUI$.and.returnValue(uiResponse$.asObservable());
  }

  const getComponentBody = () => spectator.query('#body');
  const getLoginCardElement = () => spectator.query('app-login-card');
});
