import { Component, OnInit } from '@angular/core';
import { UsersQuery } from '@core/states/user';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-login-screen',
  templateUrl: './login-screen.component.html',
  styleUrls: ['./login-screen.component.scss']
})
export class LoginScreenComponent implements OnInit {
  public isLogin$: Observable<boolean>;

  constructor(private usersQuery: UsersQuery) { }

  ngOnInit(): void {
    this.setLoginUIListener();
  }

  private setLoginUIListener(): void {
    this.isLogin$ = this.usersQuery.getLoginUI$();
  }
}
