import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User, UsersQuery, UsersService } from '@core/states/user';
import { ProgressBarService } from '@services/progress-bar.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-sign-up-card',
  templateUrl: './sign-up-card.component.html',
  styleUrls: ['./sign-up-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SignUpCardComponent implements OnInit, OnDestroy {
  public form: FormGroup;
  private onDestroy$: Subject<void> = new Subject<void>();

  constructor(private fb: FormBuilder,
    private usersQuery: UsersQuery,
    private usersService: UsersService,
    private progressBarService: ProgressBarService,
    private router: Router) { }

  ngOnInit(): void {
    this.buildForm();
    this.setProgressBarListener();
  }

  public goBackToLogin(): void {
    this.usersQuery.updateUI(true);
  }

  public signUpNewUser(): void {
    if(this.form.valid) {
      const newUser: User = { ...this.form.value };
      this.usersService.createNewUser$(newUser)
        .subscribe(() => this.router.navigate(['dashboard']));
    } else {
      this.form.updateValueAndValidity({
        onlySelf: false,
        emitEvent: true
      });
    }
  }

  private buildForm(): void {
    this.form = this.fb.group({
      name: this.fb.control(null, Validators.required),
      email: this.fb.control(null, [
        Validators.required,
        Validators.email,
      ]),
      password: this.fb.control(null, Validators.required)
    })
  }

  private setProgressBarListener(): void {
    this.progressBarService.getProgressBar$()
      .pipe(
        takeUntil(this.onDestroy$)
      )
      .subscribe((response: boolean) => this.onEnableForm(!response));
  }

  private onEnableForm(enable: boolean): void {
    enable
      ? this.form.enable({
          onlySelf: false,
          emitEvent: true
        })
      : this.form.disable({
          onlySelf: false,
          emitEvent: true
        });
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.unsubscribe();
  }
}
