import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginRoutingModule } from './login-routing.module';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IconnedButtonModule } from '@shared/iconned-button/iconned-button.module';
import { FlexLayoutModule } from '@angular/flex-layout';

import { LoginScreenComponent } from './login-screen/login-screen.component';
import { LoginCardComponent } from './login-screen/login-card/login-card.component';
import { SignUpCardComponent } from './login-screen/sign-up-card/sign-up-card.component';

@NgModule({
  declarations: [
    LoginScreenComponent,
    LoginCardComponent,
    SignUpCardComponent
  ],
  imports: [
    CommonModule,
    LoginRoutingModule,
    MatCardModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    IconnedButtonModule,
    FlexLayoutModule
  ]
})
export class LoginModule { }
